<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersonals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('users_id');
            $table->string('npk');
            $table->string('nama_lengkap');
            $table->string('panggilan')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('alamat')->nullable();
            $table->string('rt_rw')->nullable();
            $table->string('kel_desa')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kota_kab')->nullable();
            $table->string('kode_pos')->nullable();
            $table->string('agama')->nullable();
            $table->string('status')->nullable();
            $table->string('kewarganegaraan')->nullable();
            $table->string('pendidikan_terakhir')->nullable();
            $table->string('no_telp')->nullable();
            $table->string('nik_ktp')->nullable();
            $table->string('no_kk')->nullable();
            $table->string('no_npwp')->nullable();
            $table->boolean('hapus')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal');
    }
}
