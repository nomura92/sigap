<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function personal(){
        return $this->hasOne('App\Models\Personal', 'users_id', 'id');
    }

    public function keluarga(){
        return $this->hasMany('App\Models\Keluarga', 'users_id', 'id');
    }

    public function pekerjaan(){
        return $this->hasOne('App\Models\Pekerjaan', 'users_id', 'id');
    }

    public function kta(){
        return $this->hasOne('App\Models\Kta', 'users_id', 'id');
    }

    public function pendidikan(){
        return $this->hasMany('App\Models\Pendidikan', 'users_id', 'id');
    }

    public function beladiri(){
        return $this->hasMany('App\Models\Beladiri', 'users_id', 'id');
    }

    public function upload(){
        return $this->hasOne('App\Models\Upload', 'users_id', 'id');
    }

    public function image($key, $filename){
        return '<img src="'.asset('upload/'.$key.'/'.$filename).'" style="width:200px;">';
    }
}
