<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Personal;
use App\Models\Keluarga;
use App\Models\Pekerjaan;
use App\Models\Pendidikan;
use App\Models\Kta;
use App\Models\Beladiri;
use App\Models\Upload;
use Image;

class DataController extends Controller
{

	public function id(){
		$id = Auth::guard('user')->id();
		return $id;
	}

    public function index(){
    	$user = User::findOrFail(self::id());
    	return view('front.index', compact('user'));
    }

    public function index2(){
    	$user = User::findOrFail(self::id());
    	return view('front.index-2', compact('user'));
    }

    public function getPersonal(){
    	$data = Personal::whereUsersId(self::id())->first();
    	return view('front.personal', compact('data'));
    }

    public function postPersonal(Request $req){
    	$personal = Personal::whereUsersId(self::id())->first();
		$personal->nama_lengkap = $req->nama_lengkap;
		$personal->panggilan = $req->panggilan;
		$personal->tempat_lahir = $req->tempat_lahir;
		$personal->tanggal_lahir = $req->tanggal_lahir;
		$personal->jenis_kelamin = $req->jenis_kelamin;
		$personal->alamat = $req->alamat;
		$personal->rt_rw = $req->rt_rw;
		$personal->kel_desa = $req->kel_desa;
		$personal->kecamatan = $req->kecamatan;
		$personal->kota_kab = $req->kota_kab;
		$personal->kode_pos = $req->kode_pos;
		$personal->agama = $req->agama;
		$personal->status = $req->status;
		$personal->kewarganegaraan = $req->kewarganegaraan;
		$personal->pendidikan_terakhir = $req->pendidikan_terakhir;
		$personal->no_telp = $req->no_telp;
		$personal->nik_ktp = $req->nik_ktp;
		$personal->no_kk = $req->no_kk;
		$personal->no_npwp = $req->no_npwp;
		$personal->updated_at = date('Y-m-d G:i:s');
		$personal->save();

    	return redirect()->back()->with('success','Berhasil update data personal');
    }

    public function getKeluarga(){
        $personal = Personal::whereUsersId(self::id())->first();
    	$keluarga = array('ayah','ibu','pasangan','anak1','anak2','anak3');
        foreach($keluarga as $k => $v){
            $item = Keluarga::whereUsersId(self::id())->whereType($v)->first();
            $data['nama_'.$v] = array_get($item, 'nama');
            $data['umur_'.$v] = array_get($item, 'umur');
            $data['pekerjaan_'.$v] = array_get($item, 'pekerjaan');
            $data['no_telp_'.$v] = array_get($item, 'no_telp');
        }
    	return view('front.keluarga', compact('data','personal'));
    }

    public function postKeluarga(Request $req){
    	$keluarga = array('ayah','ibu','pasangan','anak1','anak2','anak3');
    	foreach($keluarga as $k => $v){
            $varnama = 'nama_'.$v;
            $varumur = 'umur_'.$v;
            $varpekerjaan = 'pekerjaan_'.$v;
            $varnotelp = 'no_telp_'.$v;
    		$cek = Keluarga::whereUsersId(self::id())->whereType($v)->first();
    		if($cek)
    			$res = Keluarga::whereUsersId(self::id())->whereType($v)->first();
    		else{
    			$res = new Keluarga;
                $res->users_id = self::id();
            }
    		$res->type = $v;
    		$res->nama = array_get($req,$varnama);
			$res->umur = array_get($req,$varumur);
			$res->pekerjaan = array_get($req,$varpekerjaan);
			$res->no_telp = array_get($req,$varnotelp);
			$res->save();
    	}
    	$data = Keluarga::whereUsersId(self::id())->get();
    	return redirect()->back()->with('success','Berhasil update data keluarga');
    }

    public function getPekerjaan(){
    	$data = Pekerjaan::whereUsersId(self::id())->first();
    	return view('front.pekerjaan', compact('data'));
    }

    public function postPekerjaan(Request $req){
        $pekerjaan = Pekerjaan::whereUsersId(self::id())->first();
        if(!$pekerjaan){
            $pekerjaan = new Pekerjaan;
            $pekerjaan->users_id = self::id();
        }
        $pekerjaan->jabatan = array_get($req,'jabatan');
        $pekerjaan->tanggal_masuk = array_get($req, 'tanggal_masuk');
        $pekerjaan->tempat_tugas = array_get($req, 'tempat_tugas');
        $pekerjaan->save();
        return redirect()->back()->with('success','Berhasil update data pekerjaan');
    }

    public function getKta(){
    	$data = Kta::whereUsersId(self::id())->first();
    	return view('front.kta', compact('data'));
    }

    public function postKta(Request $req){
        $kta = Kta::whereUsersId(self::id())->first();
        if(!$kta){
            $kta = new Kta;
            $kta->users_id = self::id();
        }
        $kta->status_kta = array_get($req, 'status_kta');
        $kta->no_reg_kta = array_get($req, 'no_reg_kta');
        $kta->masa_berlaku_kta = array_get($req, 'masa_berlaku_kta');
        $kta->save();

        return redirect()->back()->with('success','Berhasil update data kta');
    }

    public function getPendidikan(){
        $pendidikan = array('pratama', 'madya', 'utama');
        foreach($pendidikan as $k => $v){
            $item = Pendidikan::whereUsersId(self::id())->whereTypePendidikan($v)->first();
            $data['tempat_pendidikan_'.$v] = array_get($item, 'tempat_pendidikan');
            $data['no_ijazah_'.$v] = array_get($item, 'no_ijazah');
        }
    	return view('front.pendidikan', compact('data','pendidikan'));
    }

    public function postPendidikan(Request $req){
        $pendidikan = array('pratama', 'madya', 'utama');
        foreach($pendidikan as $k => $v){
            $vartempat = 'tempat_pendidikan_'.$v;
            $varno = 'no_ijazah_'.$v;
            $res = Pendidikan::whereUsersId(self::id())->whereTypePendidikan($v)->first();
            if(!$res){
                $res = new Pendidikan;
                $res->users_id = self::id();
                $res->type_pendidikan = $v;
            }
            $res->tempat_pendidikan = array_get($req, $vartempat);
            $res->no_ijazah = array_get($req, $varno);
            $res->save();
        }
        return redirect()->back()->with('success','Berhasil update data pendidikan');
    }

    public function getBeladiri(){
    	$data = Beladiri::whereUsersId(self::id())->get();
    	return view('front.beladiri', compact('data'));
    }

    public function postBeladiri(Request $req){
        Beladiri::whereUsersId(self::id())->delete();
        foreach($req->jenis_beladiri as $k => $v){
            $res = new Beladiri;
            $res->users_id = self::id();
            $res->jenis_beladiri = $v;
            $res->tingkatan = $req->tingkatan[$k];
            $res->save();
        }

        return redirect()->back()->with('success','Berhasil update data beladiri');
    }

    public function getUpload(){
    	$data = Upload::whereUsersId(self::id())->first();
    	return view('front.upload', compact('data'));
    }

    public function postUpload(Request $req){
        $upload = Upload::whereUsersId(self::id())->first();
        if(!$upload){
            $upload = new Upload;
            $upload->users_id = self::id();
        }
        if($req->hasFile('photo')){
            $upload->upload_photo = self::storeImage('photo');
        }
        if($req->hasFile('ktp')){
            $upload->upload_ktp = self::storeImage('ktp');
        }
        if($req->hasFile('kta')){
            $upload->upload_kta = self::storeImage('kta');
        }
        if($req->hasFile('kk')){
            $upload->upload_kk = self::storeImage('kk');
        }
        if($req->hasFile('npwp')){
            $upload->upload_npwp = self::storeImage('npwp');
        }

        $upload->save();

        return redirect()->back()->with('success','Berhasil update data upload');
    }

    public function storeImage($key){
        $image = request()->file($key);
        $nameImage = $key."_".self::id().'.jpg';
        if(file_exists( public_path() .'/upload/'.$key.'/'.$nameImage)){
            unlink(public_path() .'/upload/'.$key.'/'.$nameImage);
        }
        $thumbImage = Image::make($image->getRealPath())->resize(350, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $thumbPath = public_path() .'/upload/'.$key.'/'.$nameImage;
        $thumbImage->save($thumbPath);

        return $nameImage;
    }

    public function getSetting(){
        return view('front.setting');
    }

    public function postSetting(Request $req){
        $user = User::findOrFail(self::id());
        $user->password = bcrypt($req->password);
        $user->save();

        return redirect()->back()->with('success','Berhasil ganti password');
    }
}