<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function getLogin(){
    	return view('front.login');
    }

    public function postLogin(Request $req){
    	if (Auth::guard('user')->attempt(['username' => $req->username, 'password' => $req->password, 'is_Admin' => 0])) {
		    return redirect()->route('user.index');
		}
		return redirect()->route('user.login');
    }

    public function getLogout(){
    	Auth::guard('user')->logout();
		return redirect()->route('user.login');
    }
}
