<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function getLogin(){
    	return view('admin.login');
    }

    public function postLogin(Request $req){
    	if (Auth::guard('admin')->attempt(['username' => $req->username, 'password' => $req->password, 'is_Admin' => 1])) {
		    return redirect()->route('dashboard.index');
		}
		return redirect()->route('admin.login');
    }

    public function getLogout(){
    	Auth::guard('admin')->logout();
		return redirect()->route('admin.login');
    }
}
