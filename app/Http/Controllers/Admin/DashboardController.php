<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\Personal;
use App\Models\Keluarga;
use App\Models\Pekerjaan;
use App\Models\Pendidikan;
use App\Models\Kta;
use App\Models\Beladiri;
use App\Models\Upload;

use PDF;
use Excel;
use App\Exports\DataExport;

use Schema;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Personal::whereHapus(0)->paginate(15);;
        return view('admin.dashboard', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $type = "create";
        $data = collect();
        return view('admin.form', compact('type','data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'npk' => 'required|unique:users,username',
            'nama_lengkap' => 'required',
            'password' => 'required'
        ]);

        $users = new User;
        $users->username = $request->npk;
        $users->password = bcrypt($request->password);
        $users->save();

        $personal = new Personal;
        $personal->users_id = $users->id;
        $personal->npk = $users->username;
        $personal->nama_lengkap = $request->nama_lengkap;
        $personal->save();

        return redirect()->route('dashboard.index')->with('success','Berhasil menyimpan data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::findOrFail($id);
        return view('admin.detail', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = "update";
        $data = Personal::whereUsersId($id)->first();
        if(!$data)
            return redirect()->back()->with('error','Npk tidak ditemukan');
        return view('admin.form', compact('type','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            //'npk' => 'required|unique:users.npk',
            'nama_lengkap' => 'required'
        ]);

        $personal = Personal::whereUsersId($id)->first();
        $personal->nama_lengkap = $request->nama_lengkap;
        if($request->password){
            $user = User::findOrFail($id);
            $user->password = bcrypt($request->password);
            $user->save();
        }
        $personal->save();
        return redirect()->back()->with('success','Berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDelete($id){
        User::findOrFail($id)->delete();
        Personal::whereUsersId($id)->delete();
        Keluarga::whereUsersId($id)->delete();
        Pekerjaan::whereUsersId($id)->delete();
        Pendidikan::whereUsersId($id)->delete();
        Kta::whereUsersId($id)->delete();
        Beladiri::whereUsersId($id)->delete();
        Upload::whereUsersId($id)->delete();

        return redirect()->back()->with('success','Berhasil menghapus data');
    }

    public function getPdf($id){
        $data = User::findOrFail($id);
        $pdf = PDF::loadview('admin.pdf',compact('data'));
        return $pdf->download('detail_'.$data->username);
    }

    public function getExcel(){
        $data = new DataExport;
        return Excel::download( $data, 'data_'.date('Y_m_d_s').'.xlsx');
    }

    public function getTes(){

        $except = array('id','users_id','hapus','created_at','updated_at');

        $personal = new Personal;
        $keluarga = new Keluarga;
        $pekerjaan = new Pekerjaan;
        $pendidikan = new Pendidikan;
        $kta = new Kta;
        $beladiri = new Beladiri;
        $upload = new Upload;

        $arr = [];
        $ar_personal = [];
        foreach(Schema::getColumnListing($personal->getTable()) as $k => $v){
            if(!in_array($v, $except)){
                array_push($ar_personal, $v);
                array_push($arr, $v);
            }
        }
        $ar_keluarga = [];
        $keluarga_list = array('ayah','ibu','pasangan','anak1','anak2','anak3');
        foreach($keluarga_list as $k => $v){
            array_push($arr, 'nama_'.$v);
            array_push($arr, 'umur_'.$v);
            array_push($arr, 'pekerjaan_'.$v);
            array_push($arr, 'no_telp_'.$v);
        }
        $ar_pekerjaan = [];
        foreach(Schema::getColumnListing($pekerjaan->getTable()) as $k => $v){
            if(!in_array($v, $except)){
                array_push($ar_pekerjaan, $v);
                array_push($arr, $v);
            }
        }
        $ar_pendidikan = [];
        $pendidikan = array('pratama','madya','utama');
        // foreach(Schema::getColumnListing($pendidikan->getTable()) as $k => $v){
        //     if(!in_array($v, $except)){
        //         array_push($ar_pendidikan, $v);
        //         array_push($arr, $v);
        //     }
        // }
        foreach($pendidikan as $k => $v){
            array_push($arr, 'tempat_pendidikan_'.$v);
            array_push($arr, 'no_ijazah_'.$v);
        }
        $ar_kta = [];
        foreach(Schema::getColumnListing($kta->getTable()) as $k => $v){
            if(!in_array($v, $except)){
                array_push($ar_kta, $v);
                array_push($arr, $v);
            }
        }
        $ar_beladiri = [];
        // foreach(Schema::getColumnListing($beladiri->getTable()) as $k => $v){
        //     if(!in_array($v, $except)){
        //         array_push($ar_beladiri, $v);
        //         array_push($arr, $v);
        //     }
        // }
        for($i = 1; $i<=3; $i++){
            array_push($arr, 'jenis_beladiri_'.$i);
            array_push($arr, 'tingkat_'.$i);
        }
        $ar_upload = [];
        foreach(Schema::getColumnListing($upload->getTable()) as $k => $v){
            if(!in_array($v, $except)){
                array_push($ar_upload, $v);
                array_push($arr, $v);
            }
        }

        $personal = Personal::whereHapus(0)->get();
        $data = collect();
        $data->push($arr);
        foreach($personal as $k => $v){
            $user = [];
            //$keluarga = Keluarga::whereUsersId($v->users_id)->get();
            $pekerjaan = Pekerjaan::whereUsersId($v->users_id)->first();
            $kta = Kta::whereUsersId($v->users_id)->first();
            $beladiri = Beladiri::whereUsersId($v->users_id)->get();
            $upload = Upload::whereUsersId($v->users_id)->first();
            foreach($ar_personal as $k2 => $v2){
                $user[$v2] = $v->{$v2};
            }
            foreach($keluarga_list as $k2 => $v2){
                $item = Keluarga::whereUsersId($v->users_id)->whereType($v2)->first();
                $user['nama_'.$v2] = array_get($item, 'type');
                $user['umur_'.$v2] = array_get($item,'umur');
                $user['pekerjaan_'.$v2] = array_get($item, 'pekerjaan');
                $user['no_telp_'.$v2] = array_get($item, 'no_telp');
            }
            foreach($ar_pekerjaan as $k2 => $v2){
                $user[$v2] = isset($pekerjaan->{$v2}) ? $pekerjaan->{$v2} : '';
            }
            foreach(array('pratama','madya','utama') as $k2 => $v2){
                $item = Pendidikan::whereUsersId($v->users_id)->whereTypePendidikan($v2)->first();
                $user['tempat_pendidikan_'.$v2] = isset($item->tempat_pendidikan) ? $item->tempat_pendidikan : '';
                $user['no_ijazah_'.$v2] = isset($item->no_ijazah) ? $item->no_ijazah : '';
            }
            foreach($ar_kta as $k2 => $v2){
                $user[$v2] = isset($kta->{$v2}) ? $kta->{$v2} : '';
            }
            for($i=1;$i<=3;$i++){
                $user['jenis_beladiri_'.$i] = isset($beladiri[$i-1]->jenis_beladiri) ? $beladiri[$i-1]->jenis_beladiri : '';
                $user['tingkat_'.$i] = isset($beladiri[$i-1]->tingkat) ? $beladiri[$i-1]->tingkat : '';
            }
            foreach($ar_upload as $k2 => $v2){
                $user[$v2] = isset($upload->{$v2}) ? $upload->{$v2} : 'Belum ada';
            }
            $data->push(collect($user));
        }

        dd($data);

    }
}
