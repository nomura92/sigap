<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Users;
use App\Models\Personal;

class IndexController extends Controller
{
    public function index(){
    	$users = Personal::whereHapus(0)->get();
        return view('admin.dashboard', compact('users'));
    }

    public function create(){
    	return 'create';
    }

    public function edit($id){
    	return 'edit '.$id;
    }

    public function add(Request $req){
    	return 'post add';
    }

    public function update(Request $req, $id){
    	return 'update '.$id;
    }

    public function destroy($id){
    	return 'destroy '.$id;
    }
}
