<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\User;
use App\Models\Personal;
use App\Models\Keluarga;
use App\Models\Pekerjaan;
use App\Models\Pendidikan;
use App\Models\Kta;
use App\Models\Beladiri;
use App\Models\Upload;

use Schema;

class DataExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        $except = array('id','users_id','hapus','created_at','updated_at');

        $personal = new Personal;
        $keluarga = new Keluarga;
        $pekerjaan = new Pekerjaan;
        $pendidikan = new Pendidikan;
        $kta = new Kta;
        $beladiri = new Beladiri;
        $upload = new Upload;

        $arr = [];
        $ar_personal = [];
        foreach(Schema::getColumnListing($personal->getTable()) as $k => $v){
            if(!in_array($v, $except)){
                array_push($ar_personal, $v);
                array_push($arr, ucwords(str_replace('_', ' ', $v)));
            }
        }
        $ar_keluarga = [];
        $keluarga_list = array('ayah','ibu','pasangan','anak1','anak2','anak3');
        foreach($keluarga_list as $k => $v){
            array_push($arr, ucwords(str_replace('_', ' ', 'nama_'.$v)));
            array_push($arr, ucwords(str_replace('_', ' ', 'umur_'.$v)));
            array_push($arr, ucwords(str_replace('_', ' ', 'pekerjaan_'.$v)));
            array_push($arr, ucwords(str_replace('_', ' ', 'no_telp_'.$v)));
        }
        $ar_pekerjaan = [];
        foreach(Schema::getColumnListing($pekerjaan->getTable()) as $k => $v){
            if(!in_array($v, $except)){
                array_push($ar_pekerjaan, $v);
                array_push($arr, ucwords(str_replace('_', ' ', $v)));
            }
        }
        $ar_pendidikan = [];
        $pendidikan = array('pratama','madya','utama');
        // foreach(Schema::getColumnListing($pendidikan->getTable()) as $k => $v){
        //     if(!in_array($v, $except)){
        //         array_push($ar_pendidikan, $v);
        //         array_push($arr, $v);
        //     }
        // }
        foreach($pendidikan as $k => $v){
            array_push($arr, ucwords(str_replace('_', ' ', 'tempat_pendidikan_'.$v)));
            array_push($arr, ucwords(str_replace('_', ' ', 'no_ijazah_'.$v)));
        }
        $ar_kta = [];
        foreach(Schema::getColumnListing($kta->getTable()) as $k => $v){
            if(!in_array($v, $except)){
                array_push($ar_kta, $v);
                array_push($arr, ucwords(str_replace('_', ' ', $v)));
            }
        }
        $ar_beladiri = [];
        // foreach(Schema::getColumnListing($beladiri->getTable()) as $k => $v){
        //     if(!in_array($v, $except)){
        //         array_push($ar_beladiri, $v);
        //         array_push($arr, $v);
        //     }
        // }
        for($i = 1; $i<=3; $i++){
            array_push($arr, ucwords(str_replace('_', ' ', 'jenis_beladiri_'.$i)));
            array_push($arr, ucwords(str_replace('_', ' ', 'tingkat_'.$i)));
        }
        $ar_upload = [];
        foreach(Schema::getColumnListing($upload->getTable()) as $k => $v){
            if(!in_array($v, $except)){
                array_push($ar_upload, $v);
                array_push($arr, ucwords(str_replace('_', ' ', $v)));
            }
        }

        $personal = Personal::whereHapus(0)->get();
        $data = collect();
        $data->push($arr);
        foreach($personal as $k => $v){
            $user = [];
            //$keluarga = Keluarga::whereUsersId($v->users_id)->get();
            $pekerjaan = Pekerjaan::whereUsersId($v->users_id)->first();
            $kta = Kta::whereUsersId($v->users_id)->first();
            $beladiri = Beladiri::whereUsersId($v->users_id)->get();
            $upload = Upload::whereUsersId($v->users_id)->first();
            foreach($ar_personal as $k2 => $v2){
                $user[$v2] = $v->{$v2};
            }
            foreach($keluarga_list as $k2 => $v2){
                $item = Keluarga::whereUsersId($v->users_id)->whereType($v2)->first();
                $user['nama_'.$v2] = array_get($item, 'type');
                $user['umur_'.$v2] = array_get($item,'umur');
                $user['pekerjaan_'.$v2] = array_get($item, 'pekerjaan');
                $user['no_telp_'.$v2] = array_get($item, 'no_telp');
            }
            foreach($ar_pekerjaan as $k2 => $v2){
                $user[$v2] = isset($pekerjaan->{$v2}) ? $pekerjaan->{$v2} : '';
            }
            foreach(array('pratama','madya','utama') as $k2 => $v2){
                $item = Pendidikan::whereUsersId($v->users_id)->whereTypePendidikan($v2)->first();
                $user['tempat_pendidikan_'.$v2] = isset($item->tempat_pendidikan) ? $item->tempat_pendidikan : '';
                $user['no_ijazah_'.$v2] = isset($item->no_ijazah) ? $item->no_ijazah : '';
            }
            foreach($ar_kta as $k2 => $v2){
                $user[$v2] = isset($kta->{$v2}) ? $kta->{$v2} : '';
            }
            for($i=1;$i<=3;$i++){
                $user['jenis_beladiri_'.$i] = isset($beladiri[$i-1]->jenis_beladiri) ? $beladiri[$i-1]->jenis_beladiri : '';
                $user['tingkat_'.$i] = isset($beladiri[$i-1]->tingkat) ? $beladiri[$i-1]->tingkat : '';
            }
            foreach($ar_upload as $k2 => $v2){
                $user[$v2] = isset($upload->{$v2}) ? $upload->{$v2} : 'Belum ada';
            }
            $data->push(collect($user));
        }

        return $data;
    }
}
