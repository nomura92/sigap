@extends('front.layout.layout')

@section('content')
<div>
	<div class="border-bottom p-3 mb-3">
		<h4 class="">UPDATE DATA</h4>
		<a href="{{ route('user.index') }}" class="btn btn-warning">Kembali</a>
	</div>
	<div class="p-3 mb-3">
		<a href="{{ route('user.personal') }}" class="btn btn-block btn-secondary mb-3">UPDATE DATA PERSONAL</a>
		<a href="{{ route('user.keluarga') }}" class="btn btn-block btn-secondary mb-3">UPDATE DATA KELUARGA</a>
		<a href="{{ route('user.pekerjaan') }}" class="btn btn-block btn-secondary mb-3">UPDATE DATA PEKERJAAN</a>
		<a href="{{ route('user.pendidikan') }}" class="btn btn-block btn-secondary mb-3">UPDATE DATA PENDIDIKAN</a>
		<a href="{{ route('user.kta') }}" class="btn btn-block btn-secondary mb-3">UPDATE DATA KTA</a>
		<a href="{{ route('user.beladiri') }}" class="btn btn-block btn-secondary mb-3">UPDATE DATA BELADIRI</a>
		<a href="{{ route('user.upload') }}" class="btn btn-block btn-secondary mb-3">UPDATE DATA UPLOAD</a>
	</div>
</div>
@endsection