@extends('front.layout.layout')

@section('content')
<div class="container mt-3">
	<a href="{{ route('user.index2') }}" class="btn btn-warning">Kembali</a>
	<form method="post" action="{{ route('user.keluarga') }}">
		@csrf
		<div class="card mt-3">
		    <div class="card-header">Form Ayah</div>
		    <div class="card-body">
		        <div class="form-group">
		            <label>Nama Ayah</label>
		            <input type="text" name="nama_ayah" class="form-control" value="{{ array_get($data,'nama_ayah') }}">
		        </div>
		        <div class="form-group">
		            <label>Umur</label>
		            <input type="number" name="umur_ayah" class="form-control" value="{{ array_get($data,'umur_ayah') }}">
		        </div>
		        <div class="form-group">
		            <label>Pekerjaan</label>
		            <input type="text" name="pekerjaan_ayah" class="form-control" value="{{ array_get($data,'pekerjaan_ayah') }}">
		        </div>
		        <div class="form-group">
		            <label>No.Telp / Hp</label>
		            <input type="text" name="no_telp_ayah" class="form-control" value="{{ array_get($data,'no_telp_ayah') }}">
		        </div>
		    </div>
		</div>
		<div class="card mt-3">
		    <div class="card-header">Form Ibu</div>
		    <div class="card-body">
		        <div class="form-group">
		            <label>Nama Ibu</label>
		            <input type="text" name="nama_ibu" class="form-control" value="{{ array_get($data,'nama_ibu') }}">
		        </div>
		        <div class="form-group">
		            <label>Umur</label>
		            <input type="number" name="umur_ibu" class="form-control" value="{{ array_get($data,'umur_ibu') }}">
		        </div>
		        <div class="form-group">
		            <label>Pekerjaan</label>
		            <input type="text" name="pekerjaan_ibu" class="form-control" value="{{ array_get($data,'pekerjaan_ibu') }}">
		        </div>
		        <div class="form-group">
		            <label>No.Telp / Hp</label>
		            <input type="text" name="no_telp_ibu" class="form-control" value="{{ array_get($data,'no_telp_ibu') }}">
		        </div>
		    </div>
		</div>
		@if($personal->status=="Kawin")
		<div class="card mt-3">
		    <div class="card-header">Form Pasangan</div>
		    <div class="card-body">
		        <div class="form-group">
		            <label>Nama Pasangan</label>
		            <input type="text" name="nama_pasangan" class="form-control" value="{{ array_get($data,'nama_pasangan') }}">
		        </div>
		        <div class="form-group">
		            <label>Umur</label>
		            <input type="number" name="umur_pasangan" class="form-control" value="{{ array_get($data,'umur_pasangan') }}">
		        </div>
		        <div class="form-group">
		            <label>Pekerjaan</label>
		            <input type="text" name="pekerjaan_pasangan" class="form-control" value="{{ array_get($data,'pekerjaan_pasangan') }}">
		        </div>
		        <div class="form-group">
		            <label>No.Telp / Hp</label>
		            <input type="text" name="no_telp_pasangan" class="form-control" value="{{ array_get($data,'no_telp_pasangan') }}">
		        </div>
		    </div>
		</div>
		@endif
		@if(!is_null($personal->status) && $personal->status <> "Belum Kawin")
		<div class="card mt-3">
		    <div class="card-header">Form Anak 1</div>
		    <div class="card-body">
		        <div class="form-group">
		            <label>Nama Anak 1</label>
		            <input type="text" name="nama_anak1" class="form-control" value="{{ array_get($data,'nama_anak1') }}">
		        </div>
		        <div class="form-group">
		            <label>Umur</label>
		            <input type="number" name="umur_anak1" class="form-control" value="{{ array_get($data,'umur_anak1') }}">
		        </div>
		        <div class="form-group">
		            <label>Pekerjaan</label>
		            <input type="text" name="pekerjaan_anak1" class="form-control" value="{{ array_get($data,'pekerjaan_anak1') }}">
		        </div>
		        <div class="form-group">
		            <label>No.Telp / Hp</label>
		            <input type="text" name="no_telp_anak1" class="form-control" value="{{ array_get($data,'no_telp_anak1') }}">
		        </div>
		    </div>
		</div>
		<div class="card mt-3">
		    <div class="card-header">Form Anak 2</div>
		    <div class="card-body">
		        <div class="form-group">
		            <label>Nama Anak 2</label>
		            <input type="text" name="nama_anak2" class="form-control" value="{{ array_get($data,'nama_anak2') }}">
		        </div>
		        <div class="form-group">
		            <label>Umur</label>
		            <input type="number" name="umur_anak2" class="form-control" value="{{ array_get($data,'umur_anak2') }}">
		        </div>
		        <div class="form-group">
		            <label>Pekerjaan</label>
		            <input type="text" name="pekerjaan_anak2" class="form-control" value="{{ array_get($data,'pekerjaan_anak2') }}">
		        </div>
		        <div class="form-group">
		            <label>No.Telp / Hp</label>
		            <input type="text" name="no_telp_anak2" class="form-control" value="{{ array_get($data,'no_telp_anak2') }}">
		        </div>
		    </div>
		</div>
		<div class="card mt-3">
		    <div class="card-header">Form Anak 3</div>
		    <div class="card-body">
		        <div class="form-group">
		            <label>Nama Anak 3</label>
		            <input type="text" name="nama_anak3" class="form-control" value="{{ array_get($data,'nama_anak3') }}">
		        </div>
		        <div class="form-group">
		            <label>Umur</label>
		            <input type="number" name="umur_anak3" class="form-control" value="{{ array_get($data,'umur_anak3') }}">
		        </div>
		        <div class="form-group">
		            <label>Pekerjaan</label>
		            <input type="text" name="pekerjaan_anak3" class="form-control" value="{{ array_get($data,'pekerjaan_anak3') }}">
		        </div>
		        <div class="form-group">
		            <label>No.Telp / Hp</label>
		            <input type="text" name="no_telp_anak3" class="form-control" value="{{ array_get($data,'no_telp_anak3') }}">
		        </div>
		    </div>
		</div>
		@endif
		<div class="p-1 mb-3 mt-3">
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</form>
</div>
@endsection