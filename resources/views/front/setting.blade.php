@extends('front.layout.layout')

@section('content')
<div class="container mt-3">
	<a href="{{ route('user.index') }}" class="btn btn-warning">Kembali</a>
	<div class="card mt-3">
		<div class="card-header">Ganti Password</div>
		<div class="card-body">
			<form method="post" action="{{ route('user.setting') }}">
				@csrf
				<div class="form-group mb-3">
					<label>Password Baru</label>
					<input type="password" name="password" class="form-control" required>
				</div>
				<button type="submit" class="btn btn-success">Ganti</button>
			</form>
		</div>
	</div>
</div>
@endsection