@extends('front.layout.layout')

@section('content')
<div class="container mt-3">
	<a href="{{ route('user.index2') }}" class="btn btn-warning">Kembali</a>
	<form method="post" action="{{ route('user.pekerjaan') }}">
		@csrf
		<div class="card mt-3 mb-3">
			<div class="card-header">Form Pekerjaan</div>
			<div class="card-body">
				<div class="form-group">
					<label>Jabatan</label>
					<select class="form-control" name="jabatan">
						<option>-Pilih-</option>
						<option value="ASG" @if(array_get($data, 'jabatan') == "ASG") selected @endif>ASG</option>
						<option value="DANDRU" @if(array_get($data, 'jabatan') == "DANDRU") selected @endif>DANDRU</option>
					</select>
				</div>
				<div class="form-group">
					<label>Tanggal masuk</label>
					<input type="text" name="tanggal_masuk" class="form-control datepicker" value="{{ array_get($data, 'tanggal_masuk') }}">
				</div>
				<div>
					<label>Tempat Tugas</label>
					<select class="form-control" name="tempat_tugas">
						<option>-Pilih-</option>
						@foreach(config('tugas') as $k => $v)
						<option value="{{ $v }}" @if(array_get($data, 'tempat_tugas') == $v) selected @endif>{{ $v }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-success mt-3">Simpan</button>
			</div>
		</div>
	</form>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$('.datepicker').datetimepicker({format:'Y-m-d',timepicker:false});
</script>
@endsection