@extends('front.layout.layout')

@section('content')
<div>
	<div class="border-bottom p-3 mb-3">
		<h4 class="">SELAMAT DATANG, <br> {{ $user->personal()->first()->nama_lengkap }} / {{ $user->personal()->first()->npk }}</h4>
	</div>
	<div class="p-3 mb-3">
		<a href="{{ route('user.index2') }}" class="btn btn-block btn-secondary mb-3">UPDATE DATA</a>
		<a href="#" class="btn btn-block btn-secondary mb-3">DOWNLOAD FORMULIR</a>
		<a href="#" class="btn btn-block btn-secondary mb-3">PENGADUAN</a>
		<a href="#" class="btn btn-block btn-secondary mb-3">KONTAK</a>
	</div>
	<div class="p-3">
		<a href="{{ route('user.setting') }}" class="btn btn-block btn-secondary mb-3">GANTI PASSWORD</a>
		<a href="{{ route('user.logout') }}" class="btn btn-block btn-secondary mb-3">LOGOUT</a>
	</div>
</div>
@endsection