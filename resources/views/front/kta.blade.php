@extends('front.layout.layout')

@section('content')
<div class="container mt-3">
	<a href="{{ route('user.index2') }}" class="btn btn-warning">Kembali</a>
	<form method="post" action="{{ route('user.kta') }}">
		@csrf
		<div class="card mt-3 mb-3">
			<div class="card-header">Form KTA</div>
			<div class="card-body">
				<div class="form-group">
					<label>Status KTA</label>
					<select class="form-control" name="status_kta">
						<option>-Pilih-</option>
						<option value="Berlaku" @if(array_get($data, 'status_kta') == "Berlaku") selected @endif>Berlaku</option>
						<option value="Mati" @if(array_get($data, 'status_kta') == "Mati") selected @endif>Mati</option>
						<option value="Tidak Ada" @if(array_get($data, 'status_kta') == "Tidak Ada") selected @endif>Tidak Ada</option>
					</select>
				</div>
				<div class="form-group">
					<label>Nomor Registrasi KTA</label>
					<input type="text" name="no_reg_kta" class="form-control" value="{{ array_get($data, 'no_reg_kta') }}">
				</div>
				<div class="form-group">
					<label>Masa Berlaku KTA</label>
					<input type="text" name="masa_berlaku_kta" class="form-control datepicker" value="{{ array_get($data, 'tanggal_masuk') }}">
				</div>
				<button type="submit" class="btn btn-success mt-3">Simpan</button>
			</div>
		</div>
	</form>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$('.datepicker').datetimepicker({format:'Y-m-d',timepicker:false});
</script>
@endsection