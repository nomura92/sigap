@extends('front.layout.layout')

@section('content')
<div class="container mt-3">
	<a href="{{ route('user.index2') }}" class="btn btn-warning">Kembali</a>
	<form method="post" action="{{ route('user.beladiri') }}">
		@csrf
		<a href="#" class="btn btn-primary btn-block mt-3 mb-3 btn-tambah">+ Tambah beladiri</a>
		@forelse($data as $k => $v)
		<div class="card mb-3">
			<div class="card-header">Beladiri @if($k>0) <a href="#" class="removeMe">X</a> @endif</div>
			<div class="card-body">
				<div class="form-group">
					<label>Jenis Beladiri</label>
					<input type="text" name="jenis_beladiri[]" class="form-control" value="{{ $v->jenis_beladiri }}">
				</div>
				<div class="form-group">
					<label>Tingkatan</label>
					<input type="text" name="tingkatan[]" class="form-control datepicker" value="{{ $v->tingkat }}">
				</div>
			</div>
		</div>
		@empty
		<div class="card mb-3">
			<div class="card-header">Beladiri</div>
			<div class="card-body">
				<div class="form-group">
					<label>Jenis Beladiri</label>
					<input type="text" name="jenis_beladiri[]" class="form-control" value="" required>
				</div>
				<div class="form-group">
					<label>Tingkatan</label>
					<input type="text" name="tingkatan[]" class="form-control datepicker" value="" required>
				</div>
			</div>
		</div>
		@endforelse
		<div class="p-1 mb-3 mt-3" id="last">
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</form>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).on('click','.btn-tambah', function(e){
	e.preventDefault();
	html = '<div class="card mb-3"><div class="card-header">Beladiri <a href="#" class="removeMe">X</a></div><div class="card-body"><div class="form-group"><label>Nama Beladiri</label><input type="text" name="jenis_beladiri[]" class="form-control" value="" required></div><div class="form-group"><label>Tingkatan</label><input type="text" name="tingkatan[]" class="form-control datepicker" value="" required></div></div></div>';
	$('#last').prepend(html);
});
$(document).on('click','.removeMe', function(e){
	e.preventDefault();
	$(this).parent().parent().remove();
});
</script>
@endsection