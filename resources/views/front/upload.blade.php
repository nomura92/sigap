@extends('front.layout.layout')

@section('content')
<div class="container mt-3">
	<a href="{{ route('user.index2') }}" class="btn btn-warning">Kembali</a>
	<div class="card mt-3">
		<div class="card-header">Upload File</div>
		<div class="card-body">
			<form method="post" action="{{ route('user.upload') }}" enctype="multipart/form-data">
				@csrf
				<div class="custom-file @if(!isset($data->upload_photo)) mb-3 @endif">
				  <input type="file" class="custom-file-input" id="photo" name="photo" accept="image/jpg,image/jpeg">
				  <label class="custom-file-label" for="photo">Upload Photo</label>
				</div>
				@if(isset($data->upload_photo))
				<a href="#" class="btn btn-secondary mb-3 btn-sm btn-block btn-image" data-image="{{ asset('upload/photo/'.$data->upload_photo) }}">Lihat Photo</a>
				@endif
				<div class="custom-file @if(!isset($data->upload_ktp)) mb-3 @endif">
				  <input type="file" class="custom-file-input" id="ktp" name="ktp" accept="image/jpg,image/jpeg">
				  <label class="custom-file-label" for="ktp">Upload Ktp</label>
				</div>
				@if(isset($data->upload_ktp))
				<a href="#" class="btn btn-secondary mb-3 btn-sm btn-block btn-image" data-image="{{ asset('upload/ktp/'.$data->upload_ktp) }}">Lihat Ktp</a>
				@endif
				<div class="custom-file @if(!isset($data->upload_kta)) mb-3 @endif">
				  <input type="file" class="custom-file-input" id="kta" name="kta" accept="image/jpg,image/jpeg">
				  <label class="custom-file-label" for="kta">Upload Kta</label>
				</div>
				@if(isset($data->upload_kta))
				<a href="#" class="btn btn-secondary mb-3 btn-sm btn-block btn-image" data-image="{{ asset('upload/kta/'.$data->upload_kta) }}">Lihat Kta</a>
				@endif
				<div class="custom-file @if(!isset($data->upload_kk)) mb-3 @endif">
				  <input type="file" class="custom-file-input" id="kk" name="kk" accept="image/jpg,image/jpeg">
				  <label class="custom-file-label" for="kk">Upload KK</label>
				</div>
				@if(isset($data->upload_kk))
				<a href="#" class="btn btn-secondary mb-3 btn-sm btn-block btn-image" data-image="{{ asset('upload/kk/'.$data->upload_kk) }}">Lihat KK</a>
				@endif
				<div class="custom-file @if(!isset($data->upload_npwp)) mb-3 @endif">
				  <input type="file" class="custom-file-input" id="npwp" name="npwp" accept="image/jpg,image/jpeg">
				  <label class="custom-file-label" for="npwp">Upload Npwp</label>
				</div>
				@if(isset($data->upload_npwp))
				<a href="#" class="btn btn-secondary mb-3 btn-sm btn-block btn-image" data-image="{{ asset('upload/npwp/'.$data->upload_npwp) }}">Lihat NPWP</a>
				@endif
				<button type="submit" class="btn btn-success">Upload</button>
			</form>
		</div>
	</div>
</div>

<div id="modalImage" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content"><img id="tampilImage" src="" class="w-100"></div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).on('change','.custom-file-input',function(e){
    //get the file name
    var fileName = e.target.files[0].name;
    //replace the "Choose a file" label
    $(this).next('.custom-file-label').html(fileName);
});
$(document).on('click','.btn-image',function(e){
	e.preventDefault();
	$('#modalImage').modal('show');
	$('#tampilImage').attr('src',$(this).data('image'));
});
</script>
@endsection