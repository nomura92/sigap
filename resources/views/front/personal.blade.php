@extends('front.layout.layout')

@section('content')
<div class="container mt-3">
	<a href="{{ route('user.index2') }}" class="btn btn-warning">Kembali</a>
	<div class="card mt-3 mb-3">
		<div class="card-header">Form Personal</div>
		<div class="card-body">
			<form method="post" action="{{ route('user.personal') }}">
				@csrf
				<div class="form-group">
				    <label>Npk</label>
				    <input class="form-control" type="text" name="npk" value="{{ array_get($data, 'npk') }}" disabled>
				</div>
				<div class="form-group">
				    <label>Nama lengkap</label>
				    <input class="form-control" type="text" name="nama_lengkap" value="{{ array_get($data, 'nama_lengkap') }}">
				</div>
				<div class="form-group">
				    <label>Panggilan</label>
				    <input class="form-control" type="text" name="panggilan" value="{{ array_get($data, 'panggilan') }}">
				</div>
				<div class="form-group">
				    <label>Tempat lahir</label>
				    <input class="form-control" type="text" name="tempat_lahir" value="{{ array_get($data, 'tempat_lahir') }}">
				</div>
				<div class="form-group">
				    <label>Tanggal lahir</label>
				    <input class="form-control datepicker" type="text" name="tanggal_lahir" value="{{ array_get($data, 'tanggal_lahir') }}">
				</div>
				<div class="form-group">
				    <label>Jenis kelamin</label>
				    <select class="form-control" name="jenis_kelamin">
				    	<option value="">-Pilih-</option>
				    	<option value="Laki-laki" @if($data->jenis_kelamin == "Laki-laki") selected @endif>Laki-laki</option>
				    	<option value="Perempuan" @if($data->jenis_kelamin == "Perempuan") selected @endif>Perempuan</option>
				    </select>
				</div>
				<div class="form-group">
				    <label>Alamat</label>
				    <input class="form-control" type="text" name="alamat" value="{{ array_get($data, 'alamat') }}">
				</div>
				<div class="form-group">
				    <label>Rt rw</label>
				    <input class="form-control" type="text" name="rt_rw" value="{{ array_get($data, 'rt_rw') }}">
				</div>
				<div class="form-group">
				    <label>Kel desa</label>
				    <input class="form-control" type="text" name="kel_desa" value="{{ array_get($data, 'kel_desa') }}">
				</div>
				<div class="form-group">
				    <label>Kecamatan</label>
				    <input class="form-control" type="text" name="kecamatan" value="{{ array_get($data, 'kecamatan') }}">
				</div>
				<div class="form-group">
				    <label>Kota kab</label>
				    <input class="form-control" type="text" name="kota_kab" value="{{ array_get($data, 'kota_kab') }}">
				</div>
				<div class="form-group">
				    <label>Kode pos</label>
				    <input class="form-control" type="text" name="kode_pos" value="{{ array_get($data, 'kode_pos') }}">
				</div>
				<div class="form-group">
				    <label>Agama</label>
				    <input class="form-control" type="text" name="agama" value="{{ array_get($data, 'agama') }}">
				</div>
				<div class="form-group">
				    <label>Status</label>
				    <select class="form-control" name="status">
				    	<option value="">-Pilih-</option>
				    	@php $status = array('Kawin','Belum Kawin','Janda','Duda'); @endphp
				    	@foreach($status as $k => $v)
				    	<option value="{{ $v }}" @if($data->status == $v) selected @endif>{{ $v }}</option>
				    	@endforeach
				    </select>
				</div>
				<div class="form-group">
				    <label>Kewarganegaraan</label>
				    <select class="form-control" name="kewarganegaraan">
				    	<option value="">-Pilih-</option>
				    	<option value="WNI" @if($data->kewarganegaraan == "WNI") selected @endif>WNI</option>
				    	<option value="WNA" @if($data->kewarganegaraan == "WNA") selected @endif>WNA</option>
				    </select>
				</div>
				<div class="form-group">
				    <label>Pendidikan terakhir</label>
				    <input class="form-control" type="text" name="pendidikan_terakhir" value="{{ array_get($data, 'pendidikan_terakhir') }}">
				</div>
				<div class="form-group">
				    <label>No telp</label>
				    <input class="form-control" type="text" name="no_telp" value="{{ array_get($data, 'no_telp') }}">
				</div>
				<div class="form-group">
				    <label>Nik ktp</label>
				    <input class="form-control" type="text" name="nik_ktp" value="{{ array_get($data, 'nik_ktp') }}">
				</div>
				<div class="form-group">
				    <label>No kk</label>
				    <input class="form-control" type="text" name="no_kk" value="{{ array_get($data, 'no_kk') }}">
				</div>
				<div class="form-group">
				    <label>No npwp</label>
				    <input class="form-control" type="text" name="no_npwp" value="{{ array_get($data, 'no_npwp') }}">
				</div>
				<button type="submit" class="btn btn-success">Simpan</button>
			</form>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$('.datepicker').datetimepicker({format:'Y-m-d',timepicker:false});
</script>
@endsection