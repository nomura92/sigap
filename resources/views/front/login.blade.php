<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Login</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
  </head>
  <body class="text-center">
    <form class="form-signin" method="post" action="{{ route('user.login') }}">
      @csrf
      <h4 class="mb-3">PT. SIGAP PRIMA ASTRA <br>CABANG PADANG</h4> 
      <input type="text" id="inputUsername" name="username" class="form-control mb-3" placeholder="NPK" required autofocus>
      <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
      <button class="btn btn-lg btn-primary" type="submit">Login</button>
</form>
</body>
</html>
