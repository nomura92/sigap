@extends('front.layout.layout')

@section('content')
<div class="container mt-3">
	<a href="{{ route('user.index2') }}" class="btn btn-warning">Kembali</a>
	<form method="post" action="{{ route('user.pendidikan') }}">
		@csrf
		@foreach($pendidikan as $k => $v)
		<div class="card mt-3 mb-3">
			<div class="card-header">Gada {{ $v }}</div>
			<div class="card-body">
				<div class="form-group">
					<label>Tempat Pendidikan</label>
					<input type="text" name="tempat_pendidikan_{{ $v }}" class="form-control" value="{{ array_get($data, 'tempat_pendidikan_'.$v) }}">
				</div>
				<div class="form-group">
					<label>Nomor Ijazah</label>
					<input type="text" name="no_ijazah_{{ $v }}" class="form-control datepicker" value="{{ array_get($data, 'no_ijazah_'.$v) }}">
				</div>
			</div>
		</div>
		@endforeach
		<div class="p-1 mb-3 mt-3">
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</form>
</div>
@endsection