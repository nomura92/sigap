@extends('admin.theme.layout')

@section('title')
@if($type=="create") Create Data @else Update data @endif
@endsection

@section('content')
<div class="container mt-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
	    <li class="breadcrumb-item">Users-Personal</li>
	    <li class="breadcrumb-item active" aria-current="page">{{ucfirst($type)}}</li>
	  </ol>
	</nav>
	@if ($errors->any())
    <div class="alert alert-danger mt-2">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif
	<div class="card mt-3">
		<div class="card-header">{{ strtoupper($type) }}</div>
		<div class="card-body">
			@if($type=="create")
			<form method="post" action="{{ route('dashboard.store') }}">
			@csrf
			@else
			<form method="post" action="{{ route('dashboard.update', $data->users_id) }}">
			@csrf
			@method('put')
			@endif
				<div class="form-group">
					<label>Npk</label>
					<input type="text" name="npk" value="{{ array_get($data,'npk') }}" class="form-control" @if($type=="update") disabled @endif>
				</div>
				<div class="form-group">
					<label>Nama Lengkap</label>
					<input type="text" name="nama_lengkap" value="{{ array_get($data,'nama_lengkap') }}" class="form-control">
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" name="password" value="" class="form-control">
				</div>
				<button type="submit" class="btn btn-success">{{ $type }}</button>
			</form>
		</div>
	</div>
</div>
@endsection