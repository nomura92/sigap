@include('admin.theme.head')
@include('admin.theme.navbar')
@if (session('success'))
<div class="container">
	<div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
		{{ session('success') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
	  	</button>
	</div>
</div>
@endif
@yield('content')
@include('admin.theme.foot')