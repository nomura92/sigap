@extends('admin.theme.layout')

@section('title', $data->username)

@section('content')
<div class="container mt-3">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
	    <li class="breadcrumb-item">Detail</li>
	    <li class="breadcrumb-item active" aria-current="page">{{ $data->username }}</li>
	  </ol>
	</nav>

	<div class="row mb-3">
		<div class="col-md-12">
			<a href="{{ route('download.pdf', $data->id) }}" target="_blank" class="btn btn-info">Download PDF</a>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">Personal</div>
				<div class="card-body">
					<table class="table">
						<tbody>
							@php $personal = array('id','users_id','hapus','created_at','updated_at'); @endphp
							@foreach($data->personal()->first()->getAttributes() as $k => $v)
								@if(!in_array($k, $personal))
							<tr>
								<td>{{ str_replace('_',' ',ucwords($k)) }}</td> <td>{{ $v ? $v : '-'}}</td>
							</tr>
								@endif
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">Keluarga</div>
				<div class="card-body">
					<table class="table">
						<tbody>
							@php $keluarga = array('id','type','users_id','created_at','updated_at'); @endphp
							@if($data->keluarga()->count() > 0)
							@foreach($data->keluarga()->get() as $k => $v)
								<?php if($data->personal()->first()->status == "Belum Kawin" && ($v->type =="pasangan" || $v->type=="anak1" || $v->type=="anak2" || $v->type=="anak3")) continue; ?>
								<?php if(($data->personal()->first()->status == "Janda" || $data->personal()->first()->status == "Duda") && $v->type =="pasangan" ) continue; ?>
							<tr>
								<td colspan="2" style="background-color: #333; color: #fff;">{{ strtoupper($v->type) }}</td>
							</tr>
							<tr>
								<td>Nama</td>
								<td>{{ isset($v->nama) ? $v->nama : '-' }}</td>
							</tr>
							<tr>
								<td>Umur</td>
								<td>{{ isset($v->umur) ? $v->umur : '-' }}</td>
							</tr>
							<tr>
								<td>Pekerjaan</td>
								<td>{{ isset($v->pekerjaan) ? $v->pekerjaan : '-' }}</td>
							</tr>
							<tr>
								<td>Nomor Telepon</td>
								<td>{{ isset($v->no_telp) ? $v->no_telp : '-' }}</td>
							</tr>
							@endforeach
							@else
							@foreach(Schema::getColumnListing('keluarga') as $k => $v)
							@if($v == "type")
							<tr>
								<td colspan="2" style="background-color: #333; color: #fff;">{{ ucfirst($v) }}</td>
							</tr>
							@endif
								@if(!in_array($v, $keluarga))
							<tr>
								<td>{{ str_replace('_',' ',ucwords($v)) }}</td> <td>-</td>
							</tr>
								@endif
							@endforeach
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">Pekerjaan</div>
				<div class="card-body">
					<table class="table">
						<tbody>
							@php $pekerjaan = array('id','users_id','created_at','updated_at'); @endphp
							@if($data->pekerjaan()->first())
							@foreach($data->pekerjaan()->first()->getAttributes() as $k => $v)
							<tr>
								@if(!in_array($k, $pekerjaan))
								<td>{{ str_replace('_',' ',ucwords($k)) }}</td> <td>{{ $v ? $v : '-'}}</td>
								@endif
							</tr>
							@endforeach
							@else
							<tr>
								<td colspan="2">Belum Ada Data</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">KTA</div>
				<div class="card-body">
					<table class="table">
						<tbody>
							@php $kta = array('id','users_id','created_at','updated_at'); @endphp
							@if($data->kta()->first())
							@foreach($data->kta()->first()->getAttributes() as $k => $v)
								@if(!in_array($k, $kta))
							<tr>
								<td>{{ str_replace('_',' ',ucwords($k)) }}</td> <td>{{ $v ? $v : '-'}}</td>
							</tr>
								@endif
							@endforeach
							@else
							<tr>
								<td colspan="2">Belum Ada Data</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">Pendidikan</div>
				<div class="card-body">
					<table class="table">
						<tbody>
							@php $pendidikan = array('pratama','madya','utama'); @endphp
							@if($data->pendidikan()->count() > 0)
							@foreach($data->pendidikan()->get() as $k => $v)
							<tr>
								<td colspan="2">Gada {{ ucfirst($v->type_pendidikan) }}</td>
							</tr>
							<tr>
								<td>Tempat Pendidikan</td>
								<td>{{ isset($v->tempat_pendidikan) ? $v->tempat_pendidikan : '-' }}</td>
							</tr>
							<tr>
								<td>Nomor Ijazah</td>
								<td>{{ isset($v->no_ijazah) ? $v->no_ijazah : '-' }}</td>
							</tr>
							@endforeach
							@else
							<tr>
								<td colspan="2">Belum Ada Data</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">Beladiri</div>
				<div class="card-body">
					<table class="table">
						<tbody>
							@if($data->beladiri()->count() > 0)
							@foreach($data->beladiri()->get() as $k => $v)
							<tr>
								<td>Jenis Beladiri</td>
								<td>{{ isset($v->jenis_beladiri) ? $v->jenis_beladiri : '-' }}</td>
							</tr>
							<tr>
								<td>Tingkat</td>
								<td>{{ isset($v->tingkat) ? $v->tingkat : '-' }}</td>
							</tr>
							@endforeach
							@else
							<tr>
								<td colspan="2">Belum Ada Data</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">Upload</div>
				<div class="card-body">
					<table class="table">
						<tbody>
							@if($data->upload()->first())
							@php $v = $data->upload()->first(); @endphp
							<tr>
								<td>Upload Photo</td>
								<td>{!! isset($v->upload_photo) ? $data->image('photo',$v->upload_photo) : 'Belum ada' !!}</td>
							</tr>
							<tr>
								<td>Upload KTP</td>
								<td>{!! isset($v->upload_ktp) ? $data->image('ktp',$v->upload_ktp) : 'Belum ada' !!}</td>
							</tr>
							<tr>
								<td>Upload KTA</td>
								<td>{!! isset($v->upload_kta) ? $data->image('kta',$v->upload_kta) : 'Belum ada' !!}</td>
							</tr>
							<tr>
								<td>Upload KK</td>
								<td>{!! isset($v->upload_kk) ? $data->image('kk',$v->upload_kk) : 'Belum ada' !!}</td>
							</tr>
							<tr>
								<td>Upload Npwp</td>
								<td>{!! isset($v->upload_npwp) ? $data->image('npwp',$v->upload_npwp) : 'Belum ada' !!}</td>
							</tr>
							@else
							<tr>
								<td colspan="2">Belum Ada Data</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection