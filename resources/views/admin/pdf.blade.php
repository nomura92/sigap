<!DOCTYPE html>
<html>
<head>
	<title>Data Detail {{ $data->username }}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

	<div class="text-center">
		<h4>DATA DETAIL {{ $data->username }} - {{ $data->personal()->first()->nama_lengkap }}</h4>
	</div>

	<table class="table table-bordered mb-3">
		<thead>
			<tr>
				<th colspan="2">Personal</th>
			</tr>
		</thead>
		<tbody>
			@php $personal = array('id','users_id','hapus','created_at','updated_at'); @endphp
			@foreach($data->personal()->first()->getAttributes() as $k => $v)
				@if(!in_array($k, $personal))
			<tr>
				<td>{{ str_replace('_',' ',ucwords($k)) }}</td> <td>{{ $v ? $v : '-'}}</td>
			</tr>
				@endif
			@endforeach
		</tbody>
	</table>

	<table class="table table-bordered mb-3">
		<thead>
			<tr>
				<th colspan="2">Keluarga</th>
			</tr>
		</thead>
		<tbody>
			@php $keluarga = array('id','type','users_id','created_at','updated_at'); @endphp
			@if($data->keluarga()->count() > 0)
			@foreach($data->keluarga()->get() as $k => $v)
				<?php if($data->personal()->first()->status == "Belum Kawin" && ($v->type =="pasangan" || $v->type=="anak1" || $v->type=="anak2" || $v->type=="anak3")) continue; ?>
				<?php if(($data->personal()->first()->status == "Janda" || $data->personal()->first()->status == "Duda") && $v->type =="pasangan" ) continue; ?>
			<tr>
				<td colspan="2" style="background-color: #333; color: #fff;">{{ strtoupper($v->type) }}</td>
			</tr>
			<tr>
				<td>Nama</td>
				<td>{{ isset($v->nama) ? $v->nama : '-' }}</td>
			</tr>
			<tr>
				<td>Umur</td>
				<td>{{ isset($v->umur) ? $v->umur : '-' }}</td>
			</tr>
			<tr>
				<td>Pekerjaan</td>
				<td>{{ isset($v->pekerjaan) ? $v->pekerjaan : '-' }}</td>
			</tr>
			<tr>
				<td>Nomor Telepon</td>
				<td>{{ isset($v->no_telp) ? $v->no_telp : '-' }}</td>
			</tr>
			@endforeach
			@else
			@foreach(Schema::getColumnListing('keluarga') as $k => $v)
			@if($v == "type")
			<tr>
				<td colspan="2" style="background-color: #333; color: #fff;">{{ ucfirst($v) }}</td>
			</tr>
			@endif
				@if(!in_array($v, $keluarga))
			<tr>
				<td>{{ str_replace('_',' ',ucwords($v)) }}</td> <td>-</td>
			</tr>
				@endif
			@endforeach
			@endif
		</tbody>
	</table>

	<table class="table table-bordered mb-3">
		<thead>
			<tr>
				<th colspan="2">Pekerjaan</th>
			</tr>
		</thead>
		<tbody>
			@php $pekerjaan = array('id','users_id','created_at','updated_at'); @endphp
			@if($data->pekerjaan()->first())
			@foreach($data->pekerjaan()->first()->getAttributes() as $k => $v)
				@if(!in_array($k, $pekerjaan))
			<tr>
				<td>{{ str_replace('_',' ',ucwords($k)) }}</td> <td>{{ $v ? $v : '-'}}</td>
			</tr>
				@endif
			@endforeach
			@else
			<tr>
				<td colspan="2">Belum Ada Data</td>
			</tr>
			@endif
		</tbody>
	</table>

	<table class="table table-bordered mb-3">
		<thead>
			<tr>
				<th colspan="2">KTA</th>
			</tr>
		</thead>
		<tbody>
			@php $kta = array('id','users_id','created_at','updated_at'); @endphp
			@if($data->kta()->first())
			@foreach($data->kta()->first()->getAttributes() as $k => $v)
				@if(!in_array($k, $kta))
			<tr>
				<td>{{ str_replace('_',' ',ucwords($k)) }}</td> <td>{{ $v ? $v : '-'}}</td>
			</tr>
				@endif
			@endforeach
			@else
			<tr>
				<td colspan="2">Belum Ada Data</td>
			</tr>
			@endif
		</tbody>
	</table>

	<table class="table table-bordered mb-3">
		<thead>
			<tr>
				<th colspan="2">Pendidikan</th>
			</tr>
		</thead>
		<tbody>
			@php $pendidikan = array('pratama','madya','utama'); @endphp
			@if($data->pendidikan()->count() > 0)
			@foreach($data->pendidikan()->get() as $k => $v)
			<tr>
				<td colspan="2">Gada {{ ucfirst($v->type_pendidikan) }}</td>
			</tr>
			<tr>
				<td>Tempat Pendidikan</td>
				<td>{{ isset($v->tempat_pendidikan) ? $v->tempat_pendidikan : '-' }}</td>
			</tr>
			<tr>
				<td>Nomor Ijazah</td>
				<td>{{ isset($v->no_ijazah) ? $v->no_ijazah : '-' }}</td>
			</tr>
			@endforeach
			@else
			<tr>
				<td colspan="2">Belum Ada Data</td>
			</tr>
			@endif
		</tbody>
	</table>

	<table class="table table-bordered mb-3">
		<thead>
			<tr>
				<th colspan="2">Beladiri</th>
			</tr>
		</thead>
		<tbody>
			@if($data->beladiri()->count() > 0)
			@foreach($data->beladiri()->get() as $k => $v)
			<tr>
				<td>Jenis Beladiri</td>
				<td>{{ isset($v->jenis_beladiri) ? $v->jenis_beladiri : '-' }}</td>
			</tr>
			<tr>
				<td>Tingkat</td>
				<td>{{ isset($v->tingkat) ? $v->tingkat : '-' }}</td>
			</tr>
			@endforeach
			@else
			<tr>
				<td colspan="2">Belum Ada Data</td>
			</tr>
			@endif
		</tbody>
	</table>

	<table class="table table-bordered mb-3">
		<thead>
			<tr>
				<th colspan="2">Upload</th>
			</tr>
		</thead>
		<tbody>
			@if($data->upload()->first())
			@php $v = $data->upload()->first(); @endphp
			<tr>
				<td>Upload Photo</td>
				<td>{!! isset($v->upload_photo) ? $data->image('photo',$v->upload_photo) : 'Belum ada' !!}</td>
			</tr>
			<tr>
				<td>Upload KTP</td>
				<td>{!! isset($v->upload_ktp) ? $data->image('ktp',$v->upload_ktp) : 'Belum ada' !!}</td>
			</tr>
			<tr>
				<td>Upload KTA</td>
				<td>{!! isset($v->upload_kta) ? $data->image('kta',$v->upload_kta) : 'Belum ada' !!}</td>
			</tr>
			<tr>
				<td>Upload KK</td>
				<td>{!! isset($v->upload_kk) ? $data->image('kk',$v->upload_kk) : 'Belum ada' !!}</td>
			</tr>
			<tr>
				<td>Upload Npwp</td>
				<td>{!! isset($v->upload_npwp) ? $data->image('npwp',$v->upload_npwp) : 'Belum ada' !!}</td>
			</tr>
			@else
			<tr>
				<td colspan="2">Belum Ada Data</td>
			</tr>
			@endif
		</tbody>
	</table>
</body>
</html>