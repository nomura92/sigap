@extends('admin.theme.layout')

@section('title', 'Dashboard')

@section('content')
<div class="container">
	<div class="mt-3">
		<a class="btn btn-primary" href="{{ route('dashboard.create') }}">Tambah +</a>
		<a class="btn btn-info" href="{{ route('download.excel') }}">Download Excel</a>
	</div>
	<div class="table-responsive mt-3 mb-2">
		<table class="table table-bordered">
			<thead>
				<tr>
					<td>NPK</td>
					<td>Nama Lengkap</td>
					<td></td>
				</tr>
			</thead>
			<tbody>
			@forelse($users as $k => $v)
				<tr>
					<td>{{ $v->npk }}</td>
					<td>{{ $v->nama_lengkap }}</td>
					<td>
						<a href="{{ route('dashboard.show', $v->users_id) }}" class="btn btn-primary">Detail</a>
						<a href="{{ route('dashboard.edit', $v->users_id) }}" class="btn btn-primary">Edit</a>
						<a href="{{ route('delete', $v->users_id) }}" class="btn btn-primary">Hapus</a>
					</td>
				</tr>
			@empty
				<tr>
					<td colspan="4" align="center">No Data</td>
				</tr>
			@endforelse
			</tbody>
		</table>
	</div>
	{{ $users->links() }}
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css">
@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection