<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->namespace('Admin')->group(function () {
	Route::get('/', function(){
		return redirect()->route('dashboard.index');
	});
	Route::get('login','AuthController@getLogin')->name('admin.login');
	Route::post('login','AuthController@postLogin')->name('admin.login');
    Route::middleware('admin')->group(function(){
        Route::get('logout','AuthController@getLogout')->name('admin.logout');
        Route::resource('dashboard','DashboardController');
        Route::get('dashboard/delete/{id}','DashboardController@getDelete')->name('delete');
        Route::get('dashobard/download/pdf/{id}','DashboardController@getPdf')->name('download.pdf');
        Route::get('dashobard/download/excel','DashboardController@getExcel')->name('download.excel');
    });
    Route::get('dashobard/tes','DashboardController@getTes');
});

Route::namespace('User')->group(function () {
	Route::get('/', function(){
		return redirect()->route('user.index');
	});
	Route::get('login','AuthController@getLogin')->name('user.login');
	Route::post('login','AuthController@postLogin')->name('user.login');
	Route::middleware('user')->group(function(){
		Route::get('logout','AuthController@getLogout')->name('user.logout');
    	Route::get('/','DataController@index')->name('user.index');
    	Route::get('update','DataController@index2')->name('user.index2');
    	Route::get('update/personal','DataController@getPersonal')->name('user.personal');
    	Route::get('update/pendidikan','DataController@getPendidikan')->name('user.pendidikan');
    	Route::get('update/keluarga','DataController@getKeluarga')->name('user.keluarga');
    	Route::get('update/pekerjaan','DataController@getPekerjaan')->name('user.pekerjaan');
    	Route::get('update/kta','DataController@getKta')->name('user.kta');
    	Route::get('update/beladiri','DataController@getBeladiri')->name('user.beladiri');
    	Route::get('update/upload','DataController@getUpload')->name('user.upload');
    	Route::post('update/personal','DataController@postPersonal')->name('user.personal');
    	Route::post('update/pendidikan','DataController@postPendidikan')->name('user.pendidikan');
    	Route::post('update/keluarga','DataController@postKeluarga')->name('user.keluarga');
    	Route::post('update/pekerjaan','DataController@postPekerjaan')->name('user.pekerjaan');
    	Route::post('update/kta','DataController@postKta')->name('user.kta');
    	Route::post('update/beladiri','DataController@postBeladiri')->name('user.beladiri');
    	Route::post('update/upload','DataController@postUpload')->name('user.upload');
        Route::get('setting','DataController@getSetting')->name('user.setting');
        Route::post('setting','DataController@postSetting')->name('user.setting');
	});
});